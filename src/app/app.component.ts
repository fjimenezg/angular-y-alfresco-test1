import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as AlfrescoApi from 'alfresco-js-api';




declare const pdfjsLib: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit , OnInit {
  title = 'test1';
  public alfrescoJsApi = new AlfrescoApi({ provider: 'ECM', hostEcm: 'http://192.168.15.202:8080' });

  ngOnInit() {
    this.alfrescoJsApi.login('user', 'user').then(function (data) {
      console.log('API called successfully to login into Alfresco Content Services.');

    }, function (error) {
      console.log(error);
    });
    let ecmTicket = this.alfrescoJsApi.getTicketEcm();
    console.log('This is your  ECM ticket  ' + ecmTicket);

  }

  ngAfterViewInit() {
    this.alfrescoJsApi.core.childAssociationsApi.getNodeChildren('-root-', {}).then(
      function (data: any) {

        console.log("data: ", data);
        //       //var divElement = document.getElementById("result");

               for (let i = 0; i < data.list.entries.length; i++) {

                   console.log(data.list.entries[i]);

        //          /* var textElement = document.createTextNode(
        //               data.list.entries[i].entry.name +
        //               " (" +
        //               data.list.entries[i].entry.id +
        //               ")"
        //           ); */
        //          // var paragraphElement = document.createElement("p");
        //          // paragraphElement.appendChild(textElement);
        //         //  divElement.appendChild(paragraphElement);
               }
      },
      function (error: any) { console.error(error); });

  }
}
