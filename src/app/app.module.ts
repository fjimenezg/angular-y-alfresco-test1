import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlfrescoComponentComponent } from './alfresco-component/alfresco-component.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    AlfrescoComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  
    BrowserAnimationsModule
   
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
