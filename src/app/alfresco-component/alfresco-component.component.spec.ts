import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlfrescoComponentComponent } from './alfresco-component.component';

describe('AlfrescoComponentComponent', () => {
  let component: AlfrescoComponentComponent;
  let fixture: ComponentFixture<AlfrescoComponentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlfrescoComponentComponent]
    });
    fixture = TestBed.createComponent(AlfrescoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
